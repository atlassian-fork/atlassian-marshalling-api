/**
 * Contains the core types for marshalling objects to and from bytes.
 *
 * @since 1.0
 */
@ParametersAreNonnullByDefault
@ReturnValuesAreNonnullByDefault
@FieldsAreNonnullByDefault
package com.atlassian.marshalling.api;

import com.atlassian.annotations.nonnull.FieldsAreNonnullByDefault;
import com.atlassian.annotations.nonnull.ReturnValuesAreNonnullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
