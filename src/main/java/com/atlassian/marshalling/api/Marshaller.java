package com.atlassian.marshalling.api;

import com.atlassian.annotations.PublicSpi;

/**
 * Represents the ability to convert an object, of type <tt>T</tt>, to an array of {@code byte}s.
 * <p>Notes:</p>
 * <ul>
 * <li>
 * Data versioning is the responsibility of the implementation.
 * </li>
 * <li>
 * The implementation must be multi-thread safe.
 * </li>
 * </ul>
 *
 * @param <T> the type that can be converted
 * @since 1.0
 */
@FunctionalInterface
@PublicSpi
public interface Marshaller<T> {
    /**
     * Responsible for converting a object into an array of bytes.
     *
     * @param obj the object to be converted.
     * @return the array of bytes representing <tt>data</tt>
     * @throws MarshallingException if unable to perform the conversion. Implementors should not wrap all exceptions,
     *                              like {@link NullPointerException}, with this exception. It should be used to report
     *                              specific errors related to doing the marshalling, such as a checked exception
     *                              like {@link java.io.IOException}.
     */
    byte[] marshallToBytes(T obj) throws MarshallingException;
}
