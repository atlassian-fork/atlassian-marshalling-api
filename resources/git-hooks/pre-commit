#!/bin/bash
#
# Will run Checkstyle on Java files.
#
if git rev-parse --verify HEAD >/dev/null 2>&1
then
    against=HEAD
else
    # Initial commit: diff against an empty tree object
    against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

# Redirect output to stderr.
exec 1>&2

die()
{
    echo $*
    exit 1
}

repo_root=$(git rev-parse --show-toplevel)
cd "${repo_root}" || die "Unable to change directory to ${repo_root}"

checkstyle_jar="${repo_root}/resources/lib/checkstyle-6.12.1-all.jar"
checkstyle_xml="${repo_root}/resources/checkstyle-rules.xml"
java_cmd=$(git config java.command)
java_cmd=${java_cmd:-java}

[[ -f ${checkstyle_jar} ]] || die "Unable to read '${checkstyle_jar}'"
[[ -f ${checkstyle_xml} ]] || die "Unable to read '${checkstyle_xml}'"

# Ignoring failure when getting changed files, not a disaster to not check Java files
java_files=$(git diff-index --name-only --cached ${against} 2>&1 | uniq | grep --regexp='\.java$')

if [[ ${java_files} == "" ]]; then
    exit 0
fi

checkstyle_cmd="${java_cmd} -jar ${checkstyle_jar} -c ${checkstyle_xml} ${java_files}"

exec ${checkstyle_cmd}
